def add(x, y)
  x + y
end

def sum(number_array)
  return 0 if number_array == []
  number_array.reduce(:+)
end

def subtract(x, y)
  x - y
end

def multiply(factor_array)
  factor_array.reduce(:*)
end

def power(x, y)
  x**y
end

def factorial(number)
  return 1 if number == 0
  (1..number).reduce(:*)
end
