def translate(sentence)
  word_array = sentence.split
  word_array.map! { |word| pig_latinify(word) }
  word_array.join(" ")
end

def pig_latinify(word)
  word_beginning = [""]
  letter_array = word.chars
  letter_array.each_index do |idx|
    break if "aeio".include?(letter_array[idx])
    word_beginning << letter_array[idx]
    letter_array[idx] = ""
  end

  letter_array.join + word_beginning.join + "ay"
end
