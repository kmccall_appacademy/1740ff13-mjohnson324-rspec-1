def ctof(temp_c)
  temp_c * 1.8 + 32
end

def ftoc(temp_f)
  5 * (temp_f - 32) / 9
end
