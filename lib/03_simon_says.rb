def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, duplicate = 2)
  word_repeated = "#{word} " * duplicate
  word_repeated[0...-1]
end

def start_of_word(word, first_i_letters)
  word[0...first_i_letters]
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(title)
  little_words = ["and", "over", "the"]

  word_array = title.capitalize.split
  word_array.each do |word|
    word.capitalize! unless little_words.include?(word)
  end

  word_array.join(" ")
end
